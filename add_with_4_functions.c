//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
int get1()
{
    int p;
	printf("Number 1: ");
    scanf("%d",&p);
    return p;
}
int get2()
{
    int q;
	printf("Number 2: ");
    scanf("%d",&q);
    return q;
    
}
int sum(int x,int y)
{
    return x+y;
}
void display(int u,int v,int r)
{
    printf("Sum of %d and %d is %d",u,v,r);
}
void main()
{
    int a,b,c;
    a=get1();
    b=get2();
    c=sum(a,b);
    display(a,b,c);
}
//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
#include<stdlib.h>
float h,b,d,v;
void get1()
{
    printf("Enter values of h,b,d : ");
    scanf("%f%f%f",&h,&b,&d);
}
void check()
{
    if (b==0)
    {
        printf("value of 'b' cannot be zero....");
        exit(0);
    }
}
void calc()
{
    v=((h*b*d)+(d/b))/3;
}
void display()
{
    printf("Volume of tromboloid is : %f",v);
}
int main()
{
    get1();
    check();
    calc();
    display();
    return 0;
}
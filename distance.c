//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float x1,y,x2,y2,d;
void get()
{   
    scanf("%f%f",&x1,&y);
    scanf("%f%f",&x2,&y2);
}
void distance()
{
    d=sqrt(pow((x2-x1),2)+pow((y2-y),2));
}
void display()
{
    printf("Distance between (%f,%f) and (%f,%f) is %f",x1,y,x2,y2,d);
}
int main()
{
    printf("Enter Coordinates (x1,y1),(x2,y2):");
    get();
    distance();
    display();
    return 0;
}
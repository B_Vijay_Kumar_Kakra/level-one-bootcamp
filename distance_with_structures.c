//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct distance 
{
    float x1,y1,x2,y2,r;
}d;
void get1()
{
    printf("Enter the 1st coordinates: (x1,y1) :");
    scanf("%f%f",&d.x1,&d.y1);
}
void get2()
{
    printf("Enter the 2nd coordinates: (x2,y2) :");
    scanf("%f%f",&d.x2,&d.y2);
}
void display()
{
    printf("Distance Between (%f,%f) and (%f,%f) is %f  ",d.x1,d.y1,d.x2,d.y2,d.r);
}
void calc()
{
    d.r=sqrt(pow((d.x2-d.x1),2)+pow((d.y2-d.y1),2));
}
void main()
{
    get1();
    get2();
    calc();
    display();
}